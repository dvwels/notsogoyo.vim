notsogoyo.vim ([고요](http://en.wiktionary.org/wiki/고요하다))
=========================================================

Fork of [goyo.vim](https://github.com/junegunn/goyo.vim)
meant to be a simple focus mode. It does not provide a
distraction-free writing experience but zen like mode.

![](https://raw.github.com/junegunn/i/master/goyo.png)

(Color scheme: [srcery](https://github.com/srcery-colors/srcery-vim))

Note that while being a fork, it's not made to be entirely
compatible alongside Goyo, rather NotSoGoyo overrides it by
"stealing" almost all its functionality.

Installation
------------

Use your favorite plugin manager.

- [vim-plug](https://github.com/junegunn/vim-plug)
  1. Add `Plug 'https://gitlab.com/dvwels/notsogoyo.vim.git'` to .vimrc
  1. Run `:PlugInstall`

Breaking changes
-----------

- Dimension property does not accept height nor yoffset

Usage
-----

- `:Goyo`
    - Toggle Goyo
- `:Goyo [dimension]`
    - Turn on or resize Goyo
- `:Goyo!`
    - Turn Goyo off

The window can be resized with the usual `[count]<CTRL-W>` + `>`, `<` keys, and `<CTRL-W>` + `=` will resize it back to the initial size.

### Dimension expression

The expected format of a dimension expression is
`[WIDTH][XOFFSET]. `XOFFSET` should be prefixed by
`+` or `-`. Each component can be given in percentage.

```vim
" Width
Goyo 120

" In percentage
Goyo 50%

" With offsets
Goyo 50%+25%
```

Configuration
-------------

- `g:goyo_width` (default: 80)

### Callbacks

By default, statusline nor plugins about it
are temporarily disabled while in Goyo mode.

If you have other plugins that you want to disable/enable, or if you want to
change the default settings of Goyo window, you can set up custom routines
to be triggered on `GoyoEnter` and `GoyoLeave` events.

```vim
function! s:goyo_enter()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status off
    silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  endif
  set noshowmode
  set noshowcmd
  set scrolloff=999
  SignifyToggle
  " ...
endfunction

function! s:goyo_leave()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status on
    silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  endif
  set showmode
  set showcmd
  set scrolloff=5
  SignifyToggle
  " ...
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
```

More examples can be found here:
[Customization](https://github.com/junegunn/goyo.vim/wiki/Customization)

Pros.
-----

1. Works well with splits. Doesn't mess up with the current window arrangement
1. Works well with popular statusline plugins
1. Prevents accessing the empty windows around the central buffer
1. Can be closed with any of `:q[uit]`, `:clo[se]`, `:tabc[lose]`, or `:Goyo`
1. Can dynamically change the width of the window
1. Adjusts its colors when color scheme is changed
1. Realigns the window when the terminal (or window) is resized or when the size
   of the font is changed
1. Correctly hides colorcolumns and Emojis in statusline
1. Highly customizable with callbacks

License
-------

MIT

